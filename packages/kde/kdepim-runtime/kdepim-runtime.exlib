# Copyright 2009, 2012 Ingmar Vanhassel
# Copyright 2014-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]
require freedesktop-desktop freedesktop-mime gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="KDE Personal Information Management - Runtime files"
DESCRIPTION="
This package contains Akonadi - The PIM Storage Service
featuring:
- Common PIM data cache
- Concurrent access allows background activity independent of UI client
- Multi-process design
"
HOMEPAGE="http://kdepim.kde.org/akonadi/"

LICENCES="GPL-2 LGPL-2.1 FDL-1.2"
MYOPTIONS="
    etesync [[ description = [ Sync with the EteSync Groupware ] ]]
"
# Parts: agents, kioslaves, migration, plugins?, resources

KF5_MIN_VER=5.83.0
QT_MIN_VER=5.15.0

DEPENDENCIES="
    build:
        dev-libs/libxslt            [[ note = [ xsltproc ] ]]
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
        virtual/pkg-config
    build+run:
        app-crypt/qca:2[>=2.2.0][providers:qt5]
        app-pim/libkgapi[>=${PV}]
        kde/grantleetheme[>=${PV}]
        kde/libkdepim[>=${PV}]
        kde/pimcommon[>=${PV}]
        kde-frameworks/akonadi-calendar:5[>=${PV}]
        kde-frameworks/akonadi-contact:5[>=${PV}]
        kde-frameworks/akonadi-mime:5[>=${PV}]
        kde-frameworks/akonadi-notes:5[>=${PV}]
        kde-frameworks/kalarmcal:5[>=${PV}]
        kde-frameworks/kcalendarcore:5[>=5.63.0]
        kde-frameworks/kcalutils:5[>=${PV}]
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcodecs:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcontacts:5[>=5.63.0]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdav:5[>=5.72.0]
        kde-frameworks/kholidays:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kidentitymanagement:5[>=${PV}]
        kde-frameworks/kimap:5[>=${PV}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemmodels:5[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kldap:5[>=${PV}]
        kde-frameworks/kmailtransport:5[>=${PV}]
        kde-frameworks/kmbox:5[>=${PV}]
        kde-frameworks/kmime:5[>=${PV}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifyconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwallet:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        net-libs/cyrus-sasl
        server-pim/akonadi:5[>=${PV}]
        sys-auth/qtkeychain[providers:qt5]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtnetworkauth:5[>=${QT_MIN_VER}]
        x11-libs/qtspeech:5[>=${QT_MIN_VER}]
        x11-libs/qtwebengine:5[>=${QT_MIN_VER}]
        x11-libs/qtxmlpatterns:5[>=${QT_MIN_VER}]
        x11-misc/shared-mime-info[>=1.3]
        etesync? ( net-libs/libetebase )
"
# TODO: Bundles a copy of https://github.com/pipacs/o2 for the tomboy resource

# 23 out of 38 tests need a running X server
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # unwritten - http://mirror.kolabsys.com/pub/releases/
    # would also need boost
    -DCMAKE_DISABLE_FIND_PACKAGE_Libkolabxml:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=( 'etesync Etebase' )

CMAKE_SRC_CONFIGURE_TESTS+=(
    '-DKDEPIM_RUN_AKONADI_TEST:BOOL=TRUE -DKDEPIM_RUN_AKONADI_TEST:BOOL=FALSE'
)

kdepim-runtime_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

kdepim-runtime_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

