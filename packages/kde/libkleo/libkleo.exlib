# Copyright 2016-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]
require xdummy [ phase=test ] test-dbus-daemon

export_exlib_phases src_test

SUMMARY="A library providing crypto for E-Mails"

LICENCES="GPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS=""

KF5_MIN_VER=5.83.0
QT_MIN_VER=5.15.0

DEPENDENCIES="
    build+run:
        app-crypt/gpgme[>=1.13.1][qt5]
        dev-libs/boost[>=1.34.0]
        kde-frameworks/kcodecs:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemmodels:5[>=${KF5_MIN_VER}]
        kde-frameworks/kpimtextedit:5[>=${PV}]   [[ note = [ could be optional ] ]]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
"

DEFAULT_SRC_TEST_PARAMS+=( ARGS+="-E newkeyapprovaldialogtest" )

libkleo_src_test() {
    xdummy_start

    esandbox allow_net "unix:${TEMP}/keyresolvercoretest-*/S.gpg-agent*"
    esandbox allow_net --connect "unix:${TEMP}/keyresolvercoretest-*/S.gpg-agent*"

    test-dbus-daemon_run-tests

    esandbox disallow_net --connect "unix:${TEMP}/keyresolvercoretest-*/S.gpg-agent*"
    esandbox disallow_net "unix:${TEMP}/keyresolvercoretest-*/S.gpg-agent*"

    xdummy_stop
}

