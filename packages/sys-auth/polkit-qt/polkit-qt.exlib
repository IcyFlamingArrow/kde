# Copyright 2009, 2010 Ingmar Vanhassel
# Copyright 2014 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PN="${PN}-1"
MY_PNV="${MY_PN}-${PV}"

require kde.org [ subdir=${MY_PN} ] cmake

export_exlib_phases src_prepare src_install

SUMMARY="A library that allows developer to access PolicyKit-1 API with a nice Qt-style API"
HOMEPAGE="https://commits.kde.org/${MY_PN}"

LICENCES="LGPL-2.1"
SLOT="1"

MYOPTIONS="
    examples
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/glib:2
        sys-auth/polkit:1[>=0.99]
        x11-libs/qtbase:5[>=5.5.0]
    suggestion:
        sys-auth/polkit-kde-agent[>=0.99.0]     [[ description = [ KDE authentication GUI for PolicyKit ] ]]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DSYSCONF_INSTALL_DIR:PATH=/etc/
    -DBUILD_TEST:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_BUILDS=( EXAMPLES )

polkit-qt_src_prepare() {
    cmake_src_prepare

    # TODO: Turn this into a patch sent upstream (heirecka)
    edo sed -e "/POLKITQT-1_POLICY_FILES_INSTALL_DIR/s:\"\${POLKITQT-1_INSTALL_DIR}:\"/usr:" \
            -i PolkitQt-1Config.cmake.in
}

polkit-qt_src_install() {
    default

    edo pushd "${CMAKE_SOURCE}"
    emagicdocs
    edo popd
}

