# Copyright 2014-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-frameworks kde [ translations='ki18n' ]

SUMMARY="Configuration dialog for desktop notifications"
DESCRIPTION="
KNotifyConfig provides a configuration dialog for desktop notifications which
can be embedded in your application."

LICENCES="LGPL-2.1"
MYOPTIONS="
    canberra [[ description = [ Use libcanberra to preview event sounds ] ]]
    phonon   [[ description = [ Use phonon to preview event sounds ] ]]
    tts [[ description = [ Support for text to speech ] ]]

    ( canberra phonon ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        canberra? ( media-libs/libcanberra )
        phonon? ( media-libs/phonon[qt5(+)] )
        tts? ( x11-libs/qtspeech:5 )
"
# No autotests
#    test-expensive:
#        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
#        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
#        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
#        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    Canberra
    'phonon Phonon4Qt5'
    'tts Qt5TextToSpeech'
)

