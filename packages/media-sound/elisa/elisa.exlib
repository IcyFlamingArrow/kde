# Copyright 2017-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations=ki18n ]
require xdummy [ phase=test ] test-dbus-daemon
require freedesktop-desktop gtk-icon-cache

export_exlib_phases src_test pkg_postinst pkg_postrm

SUMMARY="A simple and easy to use music player"

LICENCES="
    CCPL-Attribution-ShareAlike-3.0 [[ note = [ background image ] ]]
    LGPL-3
"
SLOT="0"
MYOPTIONS="doc"

KF5_MIN_VER="5.80.0"
QT_MIN_VER="5.15.0"

DEPENDENCIES="
    build:
        doc? ( kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}] )
    build+run:
        kde-frameworks/baloo:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdeclarative:5[>=${KF5_MIN_VER}]
        kde-frameworks/kfilemetadata:5[>=${KF5_MIN_VER}][taglib]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kpackage:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        media/vlc
        x11-libs/qtbase:5[>=${QT_MIN_VER}][sql]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qtmultimedia:5[>=${QT_MIN_VER}][gstreamer]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
    run:
        kde-frameworks/kirigami:2[>=${KF5_MIN_VER}]
        x11-libs/qtgraphicaleffects:5[>=${QT_MIN_VER}]
        x11-libs/qtquickcontrols2:5[>=${QT_MIN_VER}]
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # "UPnP support is currently broken. You should probably avoid this
    # dependency."
    # https://gitlab.com/homeautomationqt/upnp-player-qt
    -DCMAKE_DISABLE_FIND_PACKAGE_UPNPQT:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=( 'doc KF5DocTools' )

# Skip failing test
DEFAULT_SRC_TEST_PARAMS+=( ARGS+="-E mediaplaylistproxymodel" )

elisa_src_test() {
    xdummy_start

    test-dbus-daemon_run-tests

    xdummy_stop
}

elisa_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

elisa_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

