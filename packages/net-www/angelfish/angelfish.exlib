# Copyright 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma-mobile kde
require freedesktop-desktop gtk-icon-cache

export_exlib_phases src_prepare pkg_postinst pkg_postrm

SUMMARY="Web browser for Plasma Mobile"

LICENCES="
    MIT     [[ note = [ src/regex-weburl/regex-weburl.js ] ]]
    || ( GPL-2 GPL-3 )
    || ( LGPL-2 LGPL-2.1 LGPL-3 )
    LGPL-2  [[ note = [ autotests ] ]]
"
SLOT="0"
MYOPTIONS=""

KF5_MIN_VER="5.84.0"
QT_MIN_VER="5.15.0"

DEPENDENCIES="
    build+run:
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/purpose:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][sql]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
        x11-libs/qtwebengine:5[>=${QT_MIN_VER}]
    run:
        kde-frameworks/kirigami:2[>=${KF5_MIN_VER}]
        x11-libs/qtfeedback:5
        x11-libs/qtquickcontrols2:5[>=${QT_MIN_VER}]
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DCMAKE_DISABLE_FIND_PACKAGE_Corrosion:BOOL=TRUE
)

angelfish_src_prepare() {
    cmake_src_prepare

    # QtFeedback's cmake config file is broken
    edo sed -e "/find_package(Qt5 /s/Feedback//" -i CMakeLists.txt
}

angelfish_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

angelfish_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

