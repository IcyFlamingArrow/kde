# Copyright 2009 Xavier Barrachina
# Copyright 2009 Erik Li
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'scribus-1.3.5.1-r2.ebuild' from Gentoo, which is:
#     Copyright 1999-2007 Gentoo Foundation

require sourceforge [ suffix=tar.xz ] cmake
require freedesktop-desktop gtk-icon-cache

SUMMARY="A Desktop Publishing application"
DESCRIPTION="
Scribus is an Open Source program that brings award-winning professional page layout with a
combination of press-ready output and new approaches to page layout. Underneath the modern and user
friendly interface, Scribus supports professional publishing features, such as CMYK color,
separations, Spot Colors, ICC color management and versatile PDF creation.
"
HOMEPAGE+=" https://www.scribus.net"

REMOTE_IDS+=" freshcode:${PN}"

UPSTREAM_CHANGELOG="https://bugs.scribus.net/changelog_page.php [[ lang = en ]]"
UPSTREAM_DOCUMENTATION="https://docs.scribus.net [[ lang = en ]]"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    3d-objects   [[ description = [ Enables importing 3D objects and embedding them in PDFs ] ]]
    debug
    image-import [[ description = [ Allows importing various image formats via GraphicsMagick ] ]]
    podofo       [[ description = [ Use PoDoFo for AI PDF import ] ]]
    spell
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"
#    [[ description = [ Use aspell for spelling support ] ]]

QT_MIN_VER=5.14.0

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        app-text/poppler[>=0.62.0]
        dev-lang/python:=[>=3]
        dev-libs/boost
        dev-libs/icu:=
        dev-libs/libxml2:2.0[>=2.6.0]
        media-libs/fontconfig[>=2.0]
        media-libs/freetype:2[>=2.3.11]
        media-libs/lcms2[>=2.1]
        media-libs/libcdr[>=0.1]
        media-libs/libpng:=[>=1.6]
        media-libs/libvisio[>=0.1]
        media-libs/tiff[>=3.6]
        net-print/cups
        office-libs/libfreehand[>=0.1]
        office-libs/libmspub[>=0.1]
        office-libs/libpagemaker
        office-libs/libqxp
        office-libs/librevenge
        office-libs/libzmf
        sys-libs/zlib
        x11-libs/cairo[>=1.14.0][X]
        x11-libs/harfbuzz[>=0.9.42]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
        x11-libs/qttools:5[>=${QT_MIN_VER}]
        3d-objects? ( dev-games/OpenSceneGraph )
        image-import? ( media-gfx/GraphicsMagick )
        podofo? ( app-text/podofo[>=0.5.0] )
        providers:ijg-jpeg? ( media-libs/jpeg:=[>=8] )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        spell? ( app-spell/hunspell:= )
    suggestion:
        app-text/ghostscript[>=8.60] [[ note = [ Export PDF files ] ]]
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DQT_PREFIX="/usr/$(exhost --target)"
    -D2GEOM_BUILD_SHARED=ON
    -DHAVE_FONTCONFIG=1
    -DHAVE_LIBZ=ON
    -DHAVE_TIFF=ON
    -DHAVE_XML=1
    -DWANT_NORPATH=ON
    -DWANT_PCH=OFF
    -DWANT_QT6=OFF
)
CMAKE_SRC_CONFIGURE_OPTION_WANTS+=(
    '!3d-objects NOOSG'
    DEBUG
    'image-import GRAPHICSMAGICK'
    'spell HUNSPELL'
)
CMAKE_SRC_CONFIGURE_OPTION_WITHS+=(
    PODOFO
)

src_prepare() {
    cmake_src_prepare

    # remove hard-disabling of cmake verbose output
    edo sed \
        -e '/CMAKE_VERBOSE_MAKEFILE/d' \
        -i "${CMAKE_SOURCE}"/CMakeLists.txt
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

