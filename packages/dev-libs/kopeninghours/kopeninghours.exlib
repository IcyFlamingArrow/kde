# Copyright 2020-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps [ group_or_user=libraries ] kde [ translations='k1i8n' ]

SUMMARY="A library for parsing and evaluating OSM opening hours expressions"
DESCRIPTION="
OSM opening hours expressions are used to describe when a feature is open/
available or closed. This format is not only used in OpenStreetMap itself, but
in various other data sources or APIs needing such a description as well.

See:
* https://wiki.openstreetmap.org/wiki/Key:opening_hours
* https://wiki.openstreetmap.org/wiki/Key:opening_hours/specification
"

LICENCES="
    BSD-3 [[ note = [ cmake files ] ]]
    CC0 [[ note = [ README, dotfiles, etc ] ]]
    || ( LGPL-2 LGPL-2.1 LGPL-3 )
"
SLOT="0"
MYOPTIONS=""

KF5_MIN_VER="5.77.0"
QT_MIN_VER="5.14.0"

DEPENDENCIES="
    build:
        sys-devel/bison
        sys-devel/flex
    build+run:
        kde-frameworks/kholidays:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # Python wrapper. Not interested in this for now, would need python and
    # boost[python].
    -DCMAKE_DISABLE_FIND_PACKAGE_PythonLibs:BOOL=TRUE
)

