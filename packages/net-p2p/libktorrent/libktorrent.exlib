# Copyright 2010, 2012 Ingmar Vanhassel
# Copyright 2016 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]

export_exlib_phases src_compile src_install

SUMMARY="Libktorrent contains all ktorrent's downloading code"
DESCRIPTION="
Libktorrent contains all the torrent downloading code, and ktorrent contains all application code
and plugins. The goal is to make libktorrent an independent library (though still closely related to
ktorrent), which can be used by other applications.
"
HOMEPAGE="https://www.kde.org/applications/internet/ktorrent/"

LICENCES="|| ( GPL-2 GPL-3 )"
SLOT="0"
MYOPTIONS="doc"

# Needs a running X server and sydbox tweakery to open the necessary ports
RESTRICT=test

KF5_MIN_VER=5.82.0
QT_MIN_VER=5.15.0

DEPENDENCIES="
    build:
        doc? ( app-doc/doxygen[dot] )
    build+run:
        app-crypt/qca:2[providers:qt5]
        dev-libs/boost[>=1.71.0]
        dev-libs/gmp:=[>=6.0.0]
        dev-libs/libgcrypt[>=1.4.5]
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/solid:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
"

libktorrent_src_compile() {
    default

    if option doc ; then
        emake docs
    fi
}

libktorrent_src_install() {
    cmake_src_install

    if option doc ; then
        insinto /usr/share/doc/${PNVR}/
        doins -r "${WORK}"/apidocs/html
    fi
}

